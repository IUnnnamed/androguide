package be.ac.umons.ProjetGL.BDS.Path;

import android.content.Context;
import be.ac.umons.ProjetGL.BDS.Location.PointOfInterest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.android.maps.GeoPoint;

public class Route {
	private String name;
    private ArrayList <PointOfInterest> route = null;
    private ArrayList <ArrayList <GeoPoint>> inter = null;
    private ArrayList <ArrayList <String>> places = null;
    private ArrayList <ArrayList <String>> moves = null;

    /**
     * Crée un objet Route représentant un itinéraire
     * avec comme list de PointOfInterest, une liste vide.
     */
    public Route () {
        this(new ArrayList <PointOfInterest> ());
    }

    /**
     * Crée un objet Route avec comme list de PointOfInterest,
     * la liste mise en argument.
     * @param list la liste des poi formant l'itineraire
     */
    public Route (ArrayList <PointOfInterest> list) {
        route = list;
        name = "";
        inter = new ArrayList<ArrayList<GeoPoint>>();
        places = new ArrayList<ArrayList<String>>();
        moves = new ArrayList<ArrayList<String>>();
    }

    /**
     * Ajoute un objet PointOfInterest a la route
     * @param poi l'obet PointOfInterest a ajouter
     */
    public void addPOI (PointOfInterest poi) {
    	route.add(poi);
    }

    /**
     * Ajoute un objet PointOfInterest a la route a une position donnée
     * @param i la position ou mettre l'objet PointOfInterest
     * @param poi l'obet PointOfInterest a ajouter
     */
    public void addPOI (int i, PointOfInterest poi) {
    	route.add(i, poi);
    }

    /**
     * Permet d'initialiser la route
     * @param sorting boolean pour savoir si il faut trier la route
     */
    public void compute (boolean sorting) {
    	if (sorting) {
    		this.sort(route);
    	}
    	for (int i = 0; i < route.size() - 1; i++) {
    		double fromLat =route.get(i).getLatitude();
    		double fromLon = route.get(i).getLongitude();
    		double toLat = route.get(i + 1).getLatitude();
    		double toLon = route.get(i + 1).getLongitude();

    		Document doc = getKML(fromLat, fromLon, toLat, toLon, route.get(i).getLocaleString());
    		inter.add(this.getGeoPoint(doc));
    		places.add(this.getPlaces(doc));
    		moves.add(this.getMoves(doc));
    	}
    }

    /**
     * Permet de recréer un objet Route avec sa description mise sous forme xml
     * @param route la route mise sous sa forme xml
     * @return un objet Route correspondant au xml
     */
    public static Route reform (String route) {
    	Route toReturn;

    	ArrayList<PointOfInterest> pois = new ArrayList<PointOfInterest>();
    	ArrayList<ArrayList<GeoPoint>> inter = new ArrayList<ArrayList<GeoPoint>>();
    	ArrayList<ArrayList<String>> places = new ArrayList<ArrayList<String>>();
    	ArrayList<ArrayList<String>> moves = new ArrayList<ArrayList<String>>();

    	Document doc = Jsoup.parse(route);
    	Elements poisE = doc.select("poi");
    	Elements interE = doc.select("inter");
    	Elements instructions = doc.select("instruction");

    	for (Element e : poisE) {
    		pois.add(PointOfInterest.reform(e.toString()));
    	}

    	for (int i = 0; i < interE.size(); i++) {
    		ArrayList<GeoPoint> gp_ = new ArrayList<GeoPoint>();
    		Elements gp = interE.get(i).select("geopoint");
    		for (Element e : gp) {
    			String[] var = e.text().split(",");
    			gp_.add(new GeoPoint(Integer.parseInt(var[0]), Integer.parseInt(var[1])));
    		}
    		inter.add(gp_);
    	}

    	for (int i = 0; i < instructions.size(); i++) {
    		ArrayList<String> place_ = new ArrayList<String>();
    		ArrayList<String> move_ = new ArrayList<String>();
    		Elements place = instructions.get(i).select("place");
    		Elements move = instructions.get(i).select("move");
    		for (int j = 0; j < place.size(); j++) {
    			place_.add(place.get(j).text());
    			move_.add(move.get(j).text());
    		}
    		places.add(place_);
    		moves.add(move_);
    	}

    	toReturn = new Route(pois);
    	toReturn.setGeoPoints(inter);
    	toReturn.setPlaces(places);
    	toReturn.setMoves(moves);

    	return toReturn;
    }

    /**
     * Permet de permuter deux elements de l'itinéraire
     * @param i la position du premier element
     * @param j la position du second element
     */
    public void swap (int i, int j) {
    	Collections.swap(route, i, j);
    }

    /**
     * Retourne le i-ème POI de l'itinéraire
     * @param index la position de l'element a renvoyer
     * @return l'objet PointOfInterest de la position "index" de l'itinéraire
     */
    public PointOfInterest get (int index) {
    	return route.get(index);
    }

    /**
     * Retourne la taille de l'itinéraire
     * @return le nombre d'element de l'itinéraire
     */
    public int size() {
    	return route.size();
    }

    /**
     * Retourne la durée total de l'itinéraire basée sur le temps que l'on met à visité un POI
     * @return la durée total de l'itinéraire
     */
    public int getDuration () {
        int duration = 0;
        for (PointOfInterest l : route) {
            duration += l.getDuration();
        }

        return duration;
    }

    /**Retourne le prix total de l'itinéraire basé sur la somme des prix de chacun des POI
     *
     * @return le prix total de l'itinéraire
     */
    public double getPrice () {
        double price = 0;
        for (PointOfInterest l : route) {
            price += l.getPrice();
        }

        return price;
    }

    /**
     * Ajoute l'objet dans l'historique
     * @param context un objet Context permettant d'écrire dans un fichier
     * @param name le nom de la route
     */
    public void addToHistory (Context context, String name) {
    	File file = new File(context.getFilesDir() + "/" + "history.xml");
    	String rest = "";
    	try {
            Document doc = Jsoup.parse(file, "UTF-8");
            Elements routes = doc.select("route");
            while(routes.size() >= 10) {
                routes.remove(0);
            }
            rest = routes.toString();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    	this.name = name;
    	FileOutputStream fOut = null;
        OutputStreamWriter osw = null;

        try {
            fOut = context.openFileOutput("history.xml",Context.MODE_PRIVATE);
            osw = new OutputStreamWriter(fOut);
            osw.write(rest + this.toString());
            osw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                osw.close();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Ajoute l'objet dans les favoris
     * @param context un objet Context permettant d'écrire dans un fichier
     * @param name le nom de la route
     */
    public void addToFavorite (Context context, String name) {
    	FileOutputStream fOut = null;
        OutputStreamWriter osw = null;
        this.name = name;
        try {
        	fOut = context.openFileOutput("favorite.xml",Context.MODE_APPEND);
        	osw = new OutputStreamWriter(fOut);
        	osw.write(this.toString());
        	osw.flush();
         } catch (Exception e) {
        	 e.printStackTrace();
         } finally {
        	 try {
        		 osw.close();
        		 fOut.close();
        	 } catch (IOException e) {
        		 e.printStackTrace();
        	 }
         }
    }

    /*
     * Permet de trier les poi de la route pour en faire un itinéraire cohérent.
     */
	private void sort (ArrayList<PointOfInterest> array) {
		if (array.size() > 1) {
			for (int i = 0; i < array.size() - 1; i++) {
				int poi1 = getMin(array, i);
				int poi2 = getMin(array, i + 1);
				double distPOI2 = PointOfInterest.distance(array.get(i), array.get(poi2));
				double iToPoi1 = PointOfInterest.realDistance(array.get(i), array.get(poi1));

				int j;
				if (iToPoi1 < 2 * distPOI2) {
					j = poi1;
				} else {
					double iToPoi2 = PointOfInterest.realDistance(array.get(i), array.get(poi2));
					j = (iToPoi1 < iToPoi2)? poi1 : poi2;
				}
				Collections.swap(array, i + 1, j);
			}
		}
	}

	/*
	 * Renvoit l'indice du poi le plus proche du poi à l'indice deb
	 */
	private int getMin (ArrayList<PointOfInterest> poi, int deb) {
		double min = Double.POSITIVE_INFINITY;
		int k = deb;
		for (int i = deb + 1; i < poi.size(); i++) {
			double next = PointOfInterest.distance(poi.get(deb), poi.get(i));
			if (min > next) {
				min = next;
				k = i;
			}
		}
		return k;
	}

	/**
	 * @return la liste des positions des étapes entre chaque objet PointOfInterest de la route
	 */
	public ArrayList<ArrayList<GeoPoint>> getGeoPoints () {
		return this.inter;
	}

	/**
	 * @return la liste des étapes entre chaque objet PointOFInterest de la route
	 */
	public ArrayList<ArrayList<String>> getPlaces() {
		return this.places;
	}

	/**
	 * @return la liste des distances à parcourir pour chaque étapes
	 * entre chaque objet PointOfInterest de la route
	 */
	public ArrayList<ArrayList<String>> getMoves() {
		return this.moves;
	}

	/*
	 * Renvoit le document KML représentant l'itinéraire de from* vers to* (avec google maps)
	 */
	private Document getKML (double fromLat, double fromLon, double toLat, double toLon, String locale) {
		try {
			String url = PointOfInterest.getUrl(fromLat, fromLon, toLat, toLon, locale);
			InputStream is = PointOfInterest.getConnection(url);
			return Jsoup.parse(is, "UTF-8", url);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return null;
	}

	private void setGeoPoints(ArrayList<ArrayList<GeoPoint>> inter) {
		this.inter = inter;
	}

	private void setPlaces(ArrayList<ArrayList<String>> places) {
		this.places = places;
	}

	private void setMoves(ArrayList<ArrayList<String>> moves) {
		this.moves = moves;
	}

	/*
	 * Renvoit l'ensemble des GeoPoint utile au tracage de l'itinéraire entre deux poi
	 * contenu dans le fichier KML représenté par "doc"
	 */
	private ArrayList<GeoPoint> getGeoPoint (Document doc) {
		ArrayList<GeoPoint> toReturn = new ArrayList<GeoPoint>();
		String[] coord = doc.select("geometrycollection").select("coordinates").first().text().split(" ");
		for (int i = 0; i < coord.length; i++) {
			String[] temp = coord[i].split(",");
			int lon = (int) (Double.parseDouble(temp[0]) * 1E6);
			int lat = (int) (Double.parseDouble(temp[1]) * 1E6);
			toReturn.add(new GeoPoint(lat, lon));
		}

		return toReturn;
	}

	/*
	 * Renvoit l'ensemble des étapes entre deux poi contenu dans le fichier KML
	 * représenté par "doc"
	 */
	private ArrayList<String> getPlaces (Document doc) {
		ArrayList<String> toReturn = new ArrayList<String>();
		Elements name = doc.select("name");
		for (int i = 1; i < name.size() - 2; i++) {
			toReturn.add(name.get(i).text());
		}
		return toReturn;
	}

	/*
	 * Renvoit l'ensemble des déplacement entre deux poi contenu dans le fichier KML
	 * représenté par "doc"
	 */
	private ArrayList<String> getMoves (Document doc) {
		ArrayList<String> toReturn = new ArrayList<String>();
		Elements move = doc.select("description");
		for (int i = 0; i < move.size() - 1; i++) {
			toReturn.add("" + extractDistance(move.get(i)));
		}
		return toReturn;
	}

	/*
	 * Extrait une distance provenant du fichier KML
	 */
	private static double extractDistance(Element description) {
		Pattern p = Pattern.compile("\\d\\.?\\d*(&#160;(km|m))");
		Matcher m = p.matcher(description.text());
		m.find();
		String[] temp = m.group(0).split("&#160;");
		double toReturn = Double.parseDouble(temp[0]);
		if(temp[1].equals("km")) {
			return toReturn * 1000;
		} else {
			return toReturn;
		}
	}

	public String toString() {
		String toReturn = "<route name=\"" + name + "\">\n";
		for (PointOfInterest l : route)
			toReturn += l.toString();
		for (ArrayList<GeoPoint> array : inter) {
			toReturn += "<inter>\n";
			for (GeoPoint g : array)
				toReturn += "<geopoint>" + g.getLatitudeE6() + "," + g.getLongitudeE6() + "</geopoint>\n";
			toReturn += "</inter>";
		}
		for (int i = 0; i < moves.size(); i++) {
			toReturn += "<instruction>\n";
			for (int j = 0; j < moves.get(i).size(); j++) {
				toReturn += "<place>" + places.get(i).get(j) + "</place>\n";
				toReturn += "<move>" + moves.get(i).get(j) + "</move>\n";
			}
			toReturn += "</instruction>\n";
		}
		return toReturn + "</route>\n";
	}
}