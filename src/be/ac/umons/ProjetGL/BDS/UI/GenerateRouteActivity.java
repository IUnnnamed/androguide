package be.ac.umons.ProjetGL.BDS.UI;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import be.ac.umons.ProjetGL.R;
import be.ac.umons.ProjetGL.BDS.DataBase.OnlineRequester;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

public class GenerateRouteActivity extends Activity {

    private EditText priceEditText;
    private EditText durationEditText;
    private CheckBox setDefault;
    private CheckBox fullRandomPOI;
    private SharedPreferences shpref;
    private EditText addEditText;
    private Button nextButton;
    private Intent intent;
    private OnlineRequester oreq;
    private ArrayList<String> tags;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generateroute);

        oreq = new OnlineRequester("http://sgl.umons.ac.be/mobilecityguide/cible.php");

        shpref = getSharedPreferences("mobilecityguide", 0);

        priceEditText = (EditText) findViewById(R.id.priceEditText);
        durationEditText = (EditText) findViewById(R.id.durationEditText);
        addEditText = (EditText) findViewById(R.id.initialAddEditText);
        setDefault = (CheckBox) findViewById(R.id.byDefaultCheckBox);
        fullRandomPOI = (CheckBox) findViewById(R.id.allRandomPOI);
        nextButton = (Button) findViewById(R.id.nextButton);

        nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    intent = new Intent(GenerateRouteActivity.this, DisplayPOIActivity.class);

                    if (fullRandomPOI.isChecked()) {
                        tags = new ArrayList<String>();
                        showDialog(1);
                    }
                    else {
                        intent.putExtra("ancestor", "generateRoute");

                        String address = addEditText.getText().toString();
                        if (!address.equals("")) {
                            try {
                                intent.putExtra("first", getLocation(address));
                                startActivity(intent);
                            } catch (Exception e) {
                                showDialog(0);
                            }
                        } else {
                            startActivity(intent);
                        }
                    }
                }
            });

        final String actualProfile = shpref.getString("profileName","Default");

        setDefault.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    if (isChecked){
                        priceEditText.setText(shpref.getString(actualProfile + "price", "30"));
                        durationEditText.setText(shpref.getString(actualProfile + "duration", "120"));
                        priceEditText.setEnabled(false);
                        durationEditText.setEnabled(false);
                    }
                    else {
                        priceEditText.setEnabled(true);
                        durationEditText.setEnabled(true);
                        priceEditText.setText("");
                        durationEditText.setText("");
                    }
                }
	    });

        setDefault.setChecked(true);

    }

    private String getLocation(String address) throws Exception {
        String url = "http://maps.google.com/maps?q=" + address.replaceAll(" ", "+") + "&output=kml";
        try {
            InputStream is = getConnection(url);
            Document doc = Jsoup.parse(is, "UTF-8", url);
            String lat = doc.select("latitude").text();
            String lon = doc.select("longitude").text();
            String name = doc.select("name").text();
            if (lat.equals("") && lon.equals(""))
                throw new Exception ("Address not recognized");

            return lat + "$" + lon + "$" + name;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private InputStream getConnection(String url) {
    	InputStream is = null;
    	try {
            URLConnection conn = new URL(url).openConnection();
            is = conn.getInputStream();
    	} catch (MalformedURLException e) {
            e.printStackTrace();
    	} catch (IOException e) {
            e.printStackTrace();
    	}
    	return is;
    }

    protected Dialog onCreateDialog( int id ) {
        switch (id) {
        case 0:
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Addresse non reconnue");
            dialog.setMessage("Voulez vous continuer sans addresse de base?");
            dialog.setPositiveButton( "Oui", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(intent);
                    }
                });
            dialog.setNegativeButton("Non", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {}
                });
            return dialog.create();
        case 1:
            try {
                oreq.connect(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<String> choice = oreq.getTags();
            final String[] choiceTag = choice.toArray(new String[choice.size()]);
            final boolean[] choiceTagBool = new boolean[choiceTag.length];
            for (int i = 0; i < choiceTag.length; i++)
                choiceTagBool[i] = false;

            AlertDialog.Builder dialog2 = new AlertDialog.Builder(this);
            dialog2.setTitle( "Tags displonible" );
            dialog2.setMultiChoiceItems( choiceTag, choiceTagBool, new OnMultiChoiceClickListener(){
                    public void onClick( DialogInterface dialog, int clicked ,boolean ceva){}}); // listener vide mais obligatoire
            dialog2.setPositiveButton( "Appliquer", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        for (int i = 0; i < choiceTag.length; i++)
                            if (choiceTagBool[i])
                                tags.add(choiceTag[i]);

                        intent.putExtra("ancestor", "random");
                        intent.putExtra("price", priceEditText.getText().toString());
                        intent.putExtra("duration", durationEditText.getText().toString());
                        intent.putStringArrayListExtra("tags", tags);
                        startActivity(intent);
                    }} );
            dialog2.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {}
                });

            return dialog2.create();
        }

        return null;
    }
}
