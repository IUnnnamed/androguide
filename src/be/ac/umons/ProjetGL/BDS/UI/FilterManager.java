package be.ac.umons.ProjetGL.BDS.UI;

import java.util.ArrayList;
import java.util.List;

import be.ac.umons.ProjetGL.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class FilterManager extends Activity {
	private final String[] filterNameBase = {"Choose", "Price", "Duration", "Rank"};
	private final String[] blank=  {""};
	private final String[] operator = {"=","<",">","<=",">=","<>"};
	private ListView list;
	private SharedPreferences shpref;
	private ArrayAdapter<String> adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.filtermanager);

	    shpref = getSharedPreferences("mobilecityguide", 0);
	    filterNameBase[0] = getString(R.string.choose);
	    final String filters = shpref.getString("filters","Rank > 3");
	    final List<String> Lfilters = new ArrayList<String>();
	    for (String el : filters.split(",")) {
	    	if (!el.equals(""))
		    	Lfilters.add(el);
	    }

	    /*
	     * Création des spinner
	     */
	    final Spinner s = (Spinner) findViewById(R.id.spinner1);
	    final Spinner s2 = (Spinner) findViewById(R.id.spinner2);
	    ArrayAdapter<String> AAnameBase = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, filterNameBase);
	    AAnameBase.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    final ArrayAdapter<String> AAblank = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, blank);
	    AAblank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    final ArrayAdapter<String> AAoperator = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, operator);
	    AAoperator.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    s.setAdapter(AAnameBase);
	    s2.setAdapter(AAblank);
	    s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
	            String item = (String) parent.getItemAtPosition(pos);
	            if (!item.equals(getString(R.string.choose))) {
	        	    s2.setAdapter(AAoperator);
	            } else {
	            	s2.setAdapter(AAblank);
	            }
	        }
	        public void onNothingSelected(AdapterView<?> parent) {
	        }
	    });

	    /*
	     * textfield pour la condition du filtre
	     */
	    final EditText limit = (EditText) findViewById(R.id.editText1);

	    /*
	     * Création de la liste des filtre + bouton qui ajoute des filter
	     */
	    list = (ListView) findViewById(R.id.filterlist);
	    adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, Lfilters);
	    adapter.setNotifyOnChange(true);
	    list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		list.setAdapter(adapter);
	    this.uncheckedAll();
	    Button create = (Button) findViewById(R.id.button1);
	    create.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (!s.getSelectedItem().toString().equals(getText(R.string.choose))) {
					if (!"".equals(limit.getText().toString())) {
						Editor shprefeditor = shpref.edit();
						String filter = s.getSelectedItem().toString() +" "+ s2.getSelectedItem().toString() +" "+ limit.getText().toString();
						if (!Lfilters.contains(filter)) {
							shprefeditor.putString("filters", filters + "," + filter);
							shprefeditor.commit();
							adapter.add(filter);
							list.setItemChecked(list.getCount() - 1, false);
						}
					} else {
						showDialog(2);
					}
				} else {
					showDialog(1);
				}
			}
	    });

	    /*
	     *  Bouton de suppression de filtre
	     */
	    Button remove = (Button) findViewById(R.id.removefilter);
	    remove.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				showDialog(0);
			}
		});
	}

	/*
	 * Crée un dialog selon un identicateur
	 * @see android.app.Activity#onCreateDialog(int)
	 */
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		switch (id) {
		/*
		 * Fenêtre de confirmation de suppression de filtres
		 */
		case 0:
			dialog.setTitle( getText(R.string.removeconfirmation));
			dialog.setPositiveButton(getText(R.string.yes), new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					Editor shprefeditor = shpref.edit();
					List<String> items = new ArrayList<String>();
					String prefFilter = "";
					for (int i = 0; i < list.getCount();i++) {
						if (list.isItemChecked(i))
							items.add(adapter.getItem(i));
						else {
							prefFilter += (prefFilter.equals(""))? "" : ",";
							prefFilter += adapter.getItem(i);
						}
					}
					delete(items);

					shprefeditor.putString("filters", prefFilter);
					shprefeditor.commit();
				}});
			dialog.setNegativeButton(getText(R.string.no), new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			return dialog.create();
		/*
		 * Si aucune catégorie n'est sélectionnée
		 */
		case 1:
			dialog.setTitle(getText(R.string.categorytofilter));
			dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			return dialog.create();
		/*
		 * Si aucune limite n'est mise
		 */
		case 2:
			dialog.setTitle(getText(R.string.nolimit));
			dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			return dialog.create();
		}
		return null;
	}

	/*
	 * Désélectionne tous les éléments de la liste
	 */
	private void uncheckedAll() {
		for(int i  = 0; i < list.getCount(); i++)
			list.setItemChecked(i, false);
	}

	/*
	 * Supprime les éléments contenu dans items de la liste
	 */
	private void delete(List<String> items) {
		for(String el : items)
			adapter.remove(el);
		uncheckedAll();
	}
}
