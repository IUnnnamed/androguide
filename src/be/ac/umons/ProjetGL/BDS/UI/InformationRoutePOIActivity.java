package be.ac.umons.ProjetGL.BDS.UI;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import be.ac.umons.ProjetGL.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class InformationRoutePOIActivity extends Activity {
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.informationroutepoi);
		ListView list = (ListView) findViewById(R.id.listinfopoi);
		TextView title = (TextView) findViewById(R.id.titleinforoute);

		String route = getIntent().getStringExtra("route");

		Document doc = Jsoup.parse(route);
		title.setText(doc.select("route").attr("name"));
		
		SimpleAdapter adapter = new SimpleAdapter(this, getInformations(doc), android.R.layout.simple_list_item_2,
				new String[]{"name", "duration"}, new int[]{android.R.id.text1, android.R.id.text2});
		list.setAdapter(adapter);
	}

	private ArrayList<HashMap<String, String>> getInformations (Document doc) {
		ArrayList<HashMap<String,String>> toReturn = new ArrayList<HashMap<String,String>>();
		Elements poi = doc.select("poi");
		
		for (Element e : poi) {
			HashMap<String, String> map = new HashMap<String, String> ();
			map.put("name", e.select("name").text());
			map.put("duration", e.select("duration").text() + "min");
			toReturn.add(map);
		}
		return toReturn;
	}
}
