package be.ac.umons.ProjetGL.BDS.UI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.android.maps.GeoPoint;

import be.ac.umons.ProjetGL.R;
import be.ac.umons.ProjetGL.BDS.DataBase.OnlineRequester;
import be.ac.umons.ProjetGL.BDS.Location.GetLocation;
import be.ac.umons.ProjetGL.BDS.Location.PointOfInterest;
import be.ac.umons.ProjetGL.BDS.Path.Route;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog.Builder;

public class DisplayPOIActivity extends Activity {

    private final Context context = this;

    private ArrayList<String> filters = new ArrayList<String>();
    private ArrayList<String> routeName = new ArrayList<String>();
    private ListView list;
    private String ancestor;
    private ArrayList<String> names = new ArrayList <String>();
    private ArrayList<PointOfInterest> pois = new ArrayList<PointOfInterest>();
    private int begin = -1;
    private SparseBooleanArray checked;
    private boolean generate;
    private boolean random;
    private String first;
    private Location myLocation;
    private OnlineRequester oreq;
    private String fileName;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        ancestor = getIntent().getStringExtra("ancestor");
        generate = "generateRoute".equals(ancestor);
        random = "random".equals(ancestor);
        first = getIntent().getStringExtra("first");

        /*
         * Si l'on n'est pas dans un freewalk, on ajoute un bouton
         * permettant de charger un ititéraire.
         */
        if(generate) {
            setContentView(R.layout.displaypoi2);
            Button load = (Button) findViewById(R.id.loadRoute);
            load.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        showDialog(2);
                    }
                });
        } else {
            setContentView(R.layout.displaypoi);
        }

        list = (ListView) findViewById(R.id.listView1);
        oreq = new OnlineRequester("http://sgl.umons.ac.be/mobilecityguide/cible.php");

        for (int i = 0; i < 9; i++) {
            myLocation = GetLocation.getLocationNetwork(this);
        }

        loadPOIs();

        Button addFilterButton = (Button) findViewById(R.id.addFilterButton);
        addFilterButton.setOnClickListener(new OnClickListener(){
                public void onClick(View v) {
                    showDialog(0);
                }});

        Button onMap = (Button)	findViewById(R.id.showInMapButton);
        if (generate){
            onMap.setText(R.string.generate);
        } else {
            onMap.setText(R.string.showInMap);
        }

        onMap.setOnClickListener(new OnClickListener(){
        	public void onClick(View v) {
                    /*
                     * Etant donné que la génération d'un itinéraire peut mettre un peu de temps,
                     * on crée un progress dialog pour faire patienter l'utilisateur.
                     */
                    final ProgressDialog progress = ProgressDialog.show(v.getContext(), getText(R.string.plzwait), getText(R.string.creatingroute), true, false);
                    new Thread((new Runnable() {
                            private Handler handler = new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        progress.dismiss();
                                    }
        			};
                            public void run() {
                                Intent intent;

                                int len = list.getCount();
                                checked = list.getCheckedItemPositions();

                                Route route = new Route();
                                for (int i = 0; i < len; i++) {
                                    if (checked.get(i)) {
                                        route.addPOI(pois.get(i));
                                    }
                                }
                                addFirst(route);
                                route.compute(generate);

                                if (generate || random){
                                    route.addToHistory(context, route.get(0).getName() +
                                                       context.getString(R.string.to) +
                                                       route.get(route.size() - 1).getName());
                                    intent = new Intent(DisplayPOIActivity.this, ItinaryActivity.class);
                                    intent.putExtra("draw", true);
                                } else {
                                    intent = new Intent(DisplayPOIActivity.this, EarthMapActivity.class);
                                    intent.putExtra("draw", false);
                                }

                                intent.putExtra("route", route.toString());
                                startActivity(intent);
                                handler.sendEmptyMessage(0);
                            }
        		})).start();
        	}
            });

        /*
         * Dans le cas on l'on a demandé un itinéraire aléatoire, le créer puis
         * simuler un click sur onMap pour tout de suite aller sur la carte.
         */
        if (random) {
            ArrayList<PointOfInterest> poi_ = (ArrayList<PointOfInterest>) pois.clone();
            Collections.shuffle(poi_);
            int maxDuration = Integer.parseInt(getIntent().getStringExtra("duration"));
            double maxPrice = Double.parseDouble(getIntent().getStringExtra("price"));
            ArrayList<String> tags = getIntent().getStringArrayListExtra("tags");
            int duration = 0;
            double price = 0;
            int i = 0;
            for(PointOfInterest poi : poi_) {
                int stateDur = poi.getDuration() + duration;
                double statePrice = poi.getPrice() + price;
                boolean goodTag = false;
                System.out.println(poi.getTags().size());
                for (String tag : poi.getTags()) {
                    System.out.println(tag);
                    if (tags.contains(tag)) {
                        goodTag = true;
                        break;
                    }
                }
                if(stateDur <= maxDuration && statePrice <= maxPrice && goodTag) {
                    duration = stateDur;
                    price = statePrice;
                    list.setItemChecked(names.indexOf(poi_.get(i).getName()), true);
                }

                i++;
            }

            onMap.performClick();
        }
    }

    /*
     * Charge laa liste des pois de la base de donnée et les mettre dans une ListView
     * à choix multiple.
     */
    private void loadPOIs(){
        try {
            oreq.connect(this);
            String[] attr = {"Name", "Description", "Address", "Rank", "Longitude", "Latitude", "Tag", "Language"};
            String[][] table = oreq.get(attr, filters);
            names = new ArrayList <String>();
            for (int i = 0; i < table.length; i++){
                int lat = (int) (Double.parseDouble(table[i][6]) * 1E6);
                int longi = (int) (Double.parseDouble(table[i][5]) * 1E6);
                GeoPoint gp = new GeoPoint(lat, longi);
                PointOfInterest poi = new PointOfInterest(table[i][0], Locale.getDefault(), gp);
                poi.setAddress(table[i][3]);
                poi.setName(table[i][1]);
                poi.setRank(Double.parseDouble(table[i][4]));

                if (i == 0 || !pois.get(pois.size() - 1).equals(poi)) {
                    poi.setDescription(table[i][2], table[i][8]);
                    poi.getTags().add(table[i][7]);
                    pois.add(poi);
                    names.add(table[i][1]);
                } else {
                    if (!pois.get(pois.size() - 1).getTags().contains(table[i][7])) {
                        pois.get(pois.size() - 1).getTags().add(table[i][7]);
                    }
                    pois.get(pois.size() - 1).setDescription(table[i][2], table[i][8]);
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, names);
            list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            list.setAdapter(adapter);
            registerForContextMenu(list);
        } catch (Exception e) {
            Toast.makeText(this, this.getText(R.string.noconnection).toString(), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /*
     * Menu contextuelle lors d'un appuis long sur un élément de la liste.
     * @see android.app.Activity#onCreateContextMenu(android.view.ContextMenu, android.view.View, android.view.ContextMenu.ContextMenuInfo)
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listView1) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(names.get(info.position));
            menu.add(0, 1, 0, getText(R.string.getdesc));
            if (first == null  && generate) {
                if (info.position != begin) {
                    menu.add(0, 2, 0, getText(R.string.setflag));
                } else {
                    menu.add(0, 3, 0, getText(R.string.removeflag));
                }
            }
            menu.add(0, 0, 0, getText(R.string.exit));
        }
    }

    /*
     * Action à effectuer selon le bouton appuié à partir du menu contextuelle.
     * @see android.app.Activity#onContextItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onContextItemSelected(MenuItem aItem) {
        AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) aItem
            .getMenuInfo();
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) list.getAdapter();
        switch (aItem.getItemId()) {
        case 0: return true;
        case 1: // get information
            String name = (String) list.getAdapter().getItem(menuInfo.position);
            PointOfInterest poi = null;
            for (int i = 0; i < pois.size() - 1; i++) {
                String name_ = pois.get(i).getName();
                if (name.equals(name_) && (i != begin || name.equals(name_.substring(0, name_.length() - 9)))) {
                    poi = pois.get(i);
                    break;
                }
            }
            if (poi != null) {
                Intent intent = new Intent(DisplayPOIActivity.this, InformationPOIActivity.class);
                intent.putExtra("actualPOI", poi.toString());
                startActivity(intent);
                return true;
            } else {
                return false;
            }
        case 2: // create flag
            int nbegin = menuInfo.position;
            checked = list.getCheckedItemPositions();
            adapter.insert(adapter.getItem(nbegin) + " **FLAG**", nbegin);
            adapter.remove(adapter.getItem(nbegin + 1));
            if (begin != -1) {
                adapter.insert(adapter.getItem(begin).substring(0, adapter.getItem(begin).length() - 9), begin);
                adapter.remove(adapter.getItem(begin + 1));
            }

            begin = nbegin;
            recheck();
            return true;
        case 3: // remove flag
            adapter.insert(adapter.getItem(begin).substring(0, adapter.getItem(begin).length() - 9), begin);
            adapter.remove(adapter.getItem(begin + 1));
            list.setItemChecked(begin, false);
            begin = -1;
        }
        return false;
    }

    /*
     * Crée un dialog selon un identicateur
     * @see android.app.Activity#onCreateDialog(int)
     */
    protected Dialog onCreateDialog(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        switch (id) {
            /*
             * Montre la liste des filtres et permet dans choisoir puis recharge les POI
             * dans la liste.
             */
        case 0:
            SharedPreferences shpref = getSharedPreferences("mobilecityguide", 0);
            final String[] choicefilter = shpref.getString("filters","Rank > 3").split(",");
            final boolean[] choicefilterbool = new boolean[choicefilter.length];
            for (int i = 0; i < choicefilter.length; i++)
                choicefilterbool[i] = false;

            dialog.setTitle(getText(R.string.AvailableFilters));
            dialog.setMultiChoiceItems( choicefilter, choicefilterbool, new OnMultiChoiceClickListener(){
                    public void onClick( DialogInterface dialog, int clicked ,boolean ceva){}}); // listener vide mais obligatoire
            dialog.setPositiveButton(getText(R.string.apply), new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        switch(which){
                        case DialogInterface.BUTTON_POSITIVE:
                            boolean select = false;
                            for (int i = 0; i < choicefilter.length; i++)
                                if (choicefilterbool[i]) {
                                    select = true;
                                    filters.add(choicefilter[i]);
                                } else
                                    filters.remove(choicefilter[i]);
                            if (select)
                                loadPOIs();
                        }
                    }} );
            return dialog.create();
            /*
             * Affiche une fenêtre ayant une liste des itinéraire contenu dans
             * le fichier séléctionné et le charge après l'appuis du bouton
             * charger.
             */
        case 1:
            final String[] choiceRoute = routeName.toArray(new String[routeName.size()]);
            dialog.setTitle(getText(R.string.availableroutes));
            dialog.setSingleChoiceItems(choiceRoute, -1, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lv = ((AlertDialog)dialog).getListView();
                        lv.setTag(new Integer(which));
                    }});
            dialog.setPositiveButton(getText(R.string.apply), new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lv = ((AlertDialog)dialog).getListView();
                        Integer selected = (Integer)lv.getTag();
                        final String file_ = fileName;
                        if(selected != null) {
                            try {
                                File file = new File(context.getFilesDir() + "/" + file_);
                                Document doc = Jsoup.parse(file, "UTF-8");
                                Elements routes = doc.select("route");
                                Element route = routes.get(selected);
                                Intent intent = new Intent (DisplayPOIActivity.this, ItinaryActivity.class);
                                intent.putExtra("route", route.toString());
                                intent.putExtra("draw", true);
                                startActivity(intent);
                            } catch (IOException ioe) {
                                Toast.makeText(context, context.getText(R.string.filenotfound).toString(), Toast.LENGTH_LONG);
                            }
                        }
                    }});
            dialog.setNeutralButton(getText(R.string.moreinfo), new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lv = ((AlertDialog)dialog).getListView();
                        Integer selected = (Integer)lv.getTag();
                        final String file_ = fileName;
                        if(selected != null) {
                            try {
                                File file = new File(context.getFilesDir() + "/" + file_);
                                Document doc = Jsoup.parse(file, "UTF-8");
                                Elements routes = doc.select("route");
                                Element route = routes.get(selected);
                                Intent intent = new Intent (DisplayPOIActivity.this, InformationRoutePOIActivity.class);
                                intent.putExtra("route", route.toString());
                                startActivity(intent);
                            } catch(IOException ioe) {
                                Toast.makeText(context, context.getText(R.string.filenotfound).toString(), 10000);
                            }
                        }
                    }
                });
            dialog.setNegativeButton(getText(R.string.exit), new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {}});

            return dialog.create();
            /*
             * Affiche une fenêtre ayant une liste des fichiers contenant des itinéraires.
             */
        case 2:
            String[] choice = {getString(R.string.favorite), getString(R.string.history)};
            final String[] choice_ = {"favorite.xml", "history.xml"};
            dialog.setTitle(getText(R.string.file));
            dialog.setSingleChoiceItems(choice, -1, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lv = ((AlertDialog)dialog).getListView();
                        lv.setTag(new Integer(which));
                    }});
            dialog.setPositiveButton(getText(R.string.apply), new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        ListView lv = ((AlertDialog)dialog).getListView();
                        Integer selected = (Integer)lv.getTag();
                        if (selected != null) {
                            setFileName(choice_[selected]);
                            try {
                                File file = new File(context.getFilesDir() + "/" + choice_[selected]);
                                Document doc = Jsoup.parse(file, "UTF-8");
                                Elements route = doc.select("route");
                                for (Element e : route) {
                                    routeName.add(e.attr("name"));
                                }
                                showDialog(1);
                            } catch (IOException ioe) {
                                Toast.makeText(context, context.getText(R.string.filenotfound).toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }});
            dialog.setNegativeButton(getText(R.string.exit), new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {}});

            return dialog.create();

        }
        return null;
    }

    /*
     * Methode permettant de modifier le fichier a selectionner pour le chargemement d'un route.
     * Necessaire pour modifier une variable a partir d'une classe interne (demandant normalement
     * des variable final)
     */
    private void setFileName(String file) {
        this.fileName = file;
    }

    /*
     * Ajoute le point de départ de l'itinéraire.
     */
    private void addFirst (Route route) {
        if (first != null) {
            String[] var = first.split("\\$");
            int lat = (int) (Double.parseDouble(var[0]) * 1E6);
            int lon = (int) (Double.parseDouble(var[1]) * 1E6);
            PointOfInterest me = new PointOfInterest("-1", Locale.getDefault(), new GeoPoint(lat, lon));
            me.setName(var[2]);
            route.addPOI(0, me);
        } else if (begin != -1) {
            ArrayAdapter<String> adapter = (ArrayAdapter<String>) list.getAdapter();
            int swapper = 0;
            for (int i = 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i).endsWith("*")) {
                    String s = adapter.getItem(begin).
                        substring(0, adapter.getItem(begin).length() - 9);
                    for (int j = 0; j < route.size(); j++) {
                        if (route.get(j).getName().equals(s)) {
                            swapper = j;
                            break;
                        }
                    }
                    break;
                }
            }
            route.swap(0, swapper);
        } else if (generate || random) {
            int lat = (int) (myLocation.getLatitude() * 1E6);
            int lon = (int) (myLocation.getLongitude() * 1E6);
            PointOfInterest me = new PointOfInterest("-1", Locale.getDefault(), new GeoPoint(lat, lon));
            me.setName(getText(R.string.user).toString());
            route.addPOI(0, me);
        }
    }

    public void getDescription(View V){
        TextView t=(TextView) V;
        int id = Integer.parseInt(t.getTag().toString());
        Toast.makeText(this, "d="+pois.get(id).getDescription(this), Toast.LENGTH_LONG).show();
        Builder build = new Builder (this);
        build.setTitle(names.get(id));
        String s = getText(R.string.descpoi) + "/n";
        build.setMessage(s+pois.get(id).getDescription(this) );
        build.setPositiveButton("OK", null);
        build.show();
    }

    /*
     * Resélectionne les éléments désélectionné lors d'une actions.
     */
    private void recheck () {
        for (int i = 0; i < list.getCount(); i++) {
            if (checked.get(i) || i == begin)
                list.setItemChecked(i, true);
        }
    }

    private int getNBChecked () {
        int toReturn = -1;
        for (int i = 0; i < list.getCount(); i++) {
            if (checked.get(i))
                toReturn++;
        }

        return toReturn;
    }
}