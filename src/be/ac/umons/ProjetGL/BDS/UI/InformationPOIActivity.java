package be.ac.umons.ProjetGL.BDS.UI;

import java.util.ArrayList;
import java.util.Collections;

import be.ac.umons.ProjetGL.R;
import be.ac.umons.ProjetGL.BDS.DataBase.OnlineRequester;
import be.ac.umons.ProjetGL.BDS.Location.PointOfInterest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class InformationPOIActivity extends Activity {
	SharedPreferences shpref;
	RatingBar rate;
	PointOfInterest p;
	Editor shprefeditor;
	String actual;
	String alreadyrate;
	
	@SuppressWarnings("unused")
	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.informationpoi);
    	
    	shpref = getSharedPreferences("mobilecityguide", 0);
    	actual = shpref.getString("profilename", "Default");
    	alreadyrate = shpref.getString(actual + "rate", "");
    	ArrayList<String> list = new ArrayList<String>();
    	Collections.addAll(list, alreadyrate.split(","));
    	
    	p = PointOfInterest.reform(getIntent().getStringExtra("actualPOI"));
    	TextView name = (TextView) findViewById(R.id.namepoi);
    	name.setText(p.getName());
    	rate = (RatingBar) findViewById(R.id.ratingpoi);
    	rate.setRating((float) p.getRank());
    	TextView titleDesc = (TextView) findViewById(R.id.descpoi);
    	TextView description = (TextView) findViewById(R.id.descpoitext);
    	description.setText(p.getDescription(this));
    	TextView titleAddress = (TextView) findViewById(R.id.addresspoi);
    	TextView address = (TextView) findViewById(R.id.addresspoitext);
    	address.setText(p.getAddress());
    	TextView titlePrice = (TextView) findViewById(R.id.pricepoi);
    	TextView price = (TextView) findViewById(R.id.pricepoitext);
    	price.setText(p.getPrice() + "€");
    	TextView titleDuration = (TextView) findViewById(R.id.durationpoi);
    	TextView duration = (TextView) findViewById(R.id.durationpoitext);
    	duration.setText(p.getDuration() + "min");
    	Button setRank = (Button) findViewById(R.id.ratePOI);
    	setRank.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				showDialog(0);
			}
    	});
    	if (list.contains(p.getId())) {
    		setRank.setClickable(false);
    	}
    }
	
	protected Dialog onCreateDialog(int id) {
		switch(id) {
		case 0:
			AlertDialog.Builder builder;
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.setrank, (ViewGroup) findViewById(R.layout.informationpoi));

			TextView text = (TextView) layout.findViewById(R.id.setrank);
			text.setTextSize(12);
			final RatingBar rate = (RatingBar) layout.findViewById(R.id.setrankpoi);
			builder = new AlertDialog.Builder(this);
			builder.setView(layout);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
	    		public void onClick(DialogInterface dialog, int which) {
	    	    	shprefeditor = shpref.edit();
	    			String newAlready = alreadyrate + p.getId();
	    			shprefeditor.putString(actual + "rate", newAlready);
	    			shprefeditor.commit();
	    			System.out.println(rate.getRating());
	    			float rating = rate.getRating();
	    			setRate(rating);
				}
			});
			return builder.create();
		}
		return null;
	}
	
	private void setRate(double rate_) {
    	OnlineRequester oreq = new OnlineRequester("http://sgl.umons.ac.be/mobilecityguide/cible.php");
		try {
			oreq.connect(this);
			p.setRank(rate_);
			System.out.println(p.getRank());
			oreq.setValue("Rank", "" + p.getRank(), p.getId());
			oreq.setValue("NBVote", "" + p.getNbVote(), p.getId());
			rate.setRating((float) p.getRank());
		} catch (Exception e) {
			Toast.makeText(this, this.getText(R.string.noconnection).toString(), Toast.LENGTH_LONG);
		}
	}
}
