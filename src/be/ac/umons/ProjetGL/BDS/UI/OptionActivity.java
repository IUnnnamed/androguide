package be.ac.umons.ProjetGL.BDS.UI;

import java.util.ArrayList;
import java.util.Arrays;

import be.ac.umons.ProjetGL.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class OptionActivity extends Activity {

	SharedPreferences shpref;
	String actualProfile;
	String actualPrice;
	String actualDuration;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.option);

        final Context context = this;
        
        shpref = getSharedPreferences("mobilecityguide", 0);
        
        final Spinner spinner = (Spinner) findViewById(R.id.profilSpinner);
        String[] existingProfile = shpref.getString("existingProfiles","Default").split(" ");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, existingProfile);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        
        
        final EditText price = (EditText) findViewById(R.id.priceEditText);
        final EditText duration = (EditText) findViewById(R.id.durationEditText);
        
        Button apply = (Button)	findViewById(R.id.applyButton);
        
        apply.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
    			Editor shprefeditor = shpref.edit();
    			String newPrice = price.getText().toString();
    			String newDuration = duration.getText().toString();
    			shprefeditor.putString(actualProfile + "price", newPrice);
    			shprefeditor.putString(actualProfile + "duration", newDuration);
    			shprefeditor.commit();
    			if (!newPrice.equals(actualPrice) || !newDuration.equals(actualDuration)){
    				Toast.makeText(getApplicationContext(), "Limitation successfuly changed", Toast.LENGTH_SHORT).show();
    				actualPrice = newPrice;
    				actualDuration = newDuration;
    			} else {
    				Toast.makeText(getApplicationContext(), "Nothing to change", Toast.LENGTH_SHORT).show();
    			}
			}
        });
        

        actualProfile = shpref.getString("profileName","Default");
        actualPrice = shpref.getString(actualProfile + "price", "30");
        actualDuration = shpref.getString(actualProfile + "duration", "120");
        
        price.setText(actualPrice);
        duration.setText(actualDuration);
        spinner.setSelection(adapter.getPosition(actualProfile));
        
        
        spinner.setOnItemSelectedListener(new OnProfileSelectedListener());
        
        final Button optionbutton = (Button) findViewById(R.id.filter);
        optionbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(OptionActivity.this, FilterManager.class);
				startActivity(intent);
			}
		});
        
        Button addProfileButton = (Button) findViewById(R.id.addProfileButton);
        addProfileButton.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				AlertDialog.Builder alert = new AlertDialog.Builder(context);
				alert.setTitle(R.string.enterProfileName);
				final EditText input = new EditText(context);
				alert.setView(input);
				alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Editable value = input.getText();
						String[] existingProfiles = shpref.getString("existingProfiles","Default").split(" ");
						ArrayList<String> existingProfilesList = new ArrayList<String>(Arrays.asList(existingProfiles));
						if (existingProfilesList.contains(value.toString())){
							Toast.makeText(context, R.string.nameAlreadyUsed, Toast.LENGTH_SHORT).show();
						} else {
			    			Editor shprefeditor = shpref.edit();
			    			shprefeditor.putString("existingProfiles", shpref.getString("existingProfiles","Default") +" " + value.toString());
			    			shprefeditor.commit();
			    	        String[] existingProfile = shpref.getString("existingProfiles","Default").split(" ");
			    	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, existingProfile);
			    	        spinner.setAdapter(adapter);
			    	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			    	        spinner.setSelection(adapter.getPosition(actualProfile));
						}
					}
				});
				alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog, int whichButton) {
				  }
				});
				alert.show();
			}
        });
        
        Button deleteProfileButton = (Button) findViewById(R.id.deleteProfileButton);
        deleteProfileButton.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				final CharSequence[] items = shpref.getString("existingProfiles","Default").split(" ");
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(R.string.selectOneProfile);
				builder.setItems(items, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int pos) {
		    			Editor shprefeditor = shpref.edit();
		    			items[pos] = items[items.length-1];
		    			StringBuffer sb = new StringBuffer();
		    			for (int i = 0; i < items.length - 1; i++){
		    				sb.append(items[i]);
		    			}
		    			shprefeditor.putString("existingProfiles", sb.toString());
		    			shprefeditor.remove(items[pos]+"price");
		    			shprefeditor.remove(items[pos]+"duration");
		    			shprefeditor.commit();
		    	        String[] existingProfile = shpref.getString("existingProfiles","Default").split(" ");
		    	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, existingProfile);
		    	        spinner.setAdapter(adapter);
		    	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    	        spinner.setSelection(adapter.getPosition(actualProfile));
				    }
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
        });
    }
    
    
    public class OnProfileSelectedListener implements OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
    		String newProfile = (String) parent.getItemAtPosition(pos);
    		
    		if (!newProfile.equals(actualProfile)){
    			Editor shprefeditor = shpref.edit();
    			shprefeditor.putString("profileName", newProfile);
    			shprefeditor.commit();
	        	actualProfile = newProfile;
	        	Toast.makeText(getApplicationContext(), "Profile successfuly changed to "+newProfile+" !", Toast.LENGTH_SHORT).show();
	            actualProfile = shpref.getString("profileName","Default");
	            actualPrice = shpref.getString(actualProfile + "price", "30");
	            actualDuration = shpref.getString(actualProfile + "duration", "120");
	            EditText price = (EditText) findViewById(R.id.priceEditText);
	            EditText duration = (EditText) findViewById(R.id.durationEditText);
	            price.setText(actualPrice);
	            duration.setText(actualDuration);

    		}

        }

        public void onNothingSelected(AdapterView<?> parent) {
          // Do nothing.
        }
    }
    
 
}