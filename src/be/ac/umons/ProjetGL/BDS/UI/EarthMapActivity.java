package be.ac.umons.ProjetGL.BDS.UI;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import be.ac.umons.ProjetGL.R;
import be.ac.umons.ProjetGL.BDS.Location.GetLocation;
import be.ac.umons.ProjetGL.BDS.Location.PointOfInterest;
import be.ac.umons.ProjetGL.BDS.Location.PositionPOIList;
import be.ac.umons.ProjetGL.BDS.Location.RouteOverlay;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class EarthMapActivity extends MapActivity {
	
	PositionPOIList positionPOIsList = null;
	PointOfInterest poislist = null;
	private String route;
	private MapView mapView;
	private MapController mc;
	private GeoPoint location;
	private boolean mustDraw;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.earthmap);

		mapView =  (MapView) this.findViewById(R.id.mapView);
		mapView.setBuiltInZoomControls(true);
		mc = mapView.getController();
		mc.setZoom(10);
		
		mapView.setSatellite(false);
		mapView.setStreetView(true);
		mapView.displayZoomControls(true);
		mapView.setClickable(true);
		mapView.setReticleDrawMode(MapView.ReticleDrawMode.DRAW_RETICLE_OVER);

		/*
		 * Va prendre l'itinéraire à afficher et centre la map sur le premier
		 * POI de cette itinéraire
		 */
		if (getIntent().getExtras() != null){
			route = getIntent().getExtras().getString("route");
			GeoPoint center = getFirstGeoPoint();
			if (center == null) {
				setLocation(GetLocation.getLocationNetwork(this));
			} else {
				setLocation(getFirstGeoPoint());
			}
		} else {
			setLocation(GetLocation.getLocationNetwork(this));
		}
		
		loadPOIs();
	}

	public GeoPoint getLocation() {
		return location;
	}
	
	/*
	 * Renvoit la position du premier POI de l'itinéraire.
	 */
	private GeoPoint getFirstGeoPoint () {
		Elements pois = Jsoup.parse(route).select("poi");
		if (pois.size() > 0) {
		String first = Jsoup.parse(route).select("poi").get(0).toString();
		PointOfInterest poi = PointOfInterest.reform(first);
		return poi.getGeoPoint();
		} else {
			return null;
		}
	}

	/*
	 * Centre la carte sur la position donnée (en utilisant un GeoPoint)
	 */
	private void setLocation(GeoPoint location) {
		this.location = location;
		this.mc.setCenter(this.location);
		this.mapView.invalidate();
	}
	
	/*
	 * Centre la carte sur la position donnée (en utilisant un Location)
	 */
	private void setLocation(Location location) {
		int lat = (int) (location.getLatitude() * 1E6);
		int lon = (int) (location.getLongitude() * 1E6);
		setLocation(new GeoPoint(lat, lon));
	}

	/*
	 * Renvoit si l'itinéraire est affiché
	 * @see com.google.android.maps.MapActivity#isRouteDisplayed()
	 */
	public boolean isRouteDisplayed() {
		return mustDraw;
	}
	
	/*
	 * Affiche l'itinéraire sur la carte
	 */
	public void drawRoute() {
		Document doc = Jsoup.parse(route);
		Elements inter = doc.select("inter");
		for (Element e : inter) {
			Elements geopoints = e.select("geopoint");
			ArrayList<GeoPoint> gp = new ArrayList<GeoPoint>();
			for (Element f : geopoints) {
				String[] var = f.text().split(",");
				gp.add(new GeoPoint(Integer.parseInt(var[0]),Integer.parseInt(var[1])));
			}
			mapView.getOverlays().add(new RouteOverlay(gp));
		}
		mustDraw = true;
		mapView.invalidate();
	}
	
	/*
	 * Efface l'itinéraire
	 */
	public void undrawRoute() {
		List <Overlay> list = mapView.getOverlays();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) instanceof RouteOverlay) {
				list.remove(i);
				i--;
			}
		}
		mustDraw = false;
		mapView.invalidate();
	}
	
	/*
	 * Charge les POI de l'itinéraire et les affiches sur la carte
	 */
	private void loadPOIs(){
		if (getIntent().getExtras() != null){
			mustDraw = getIntent().getExtras().getBoolean("draw", false);
			Elements pois = Jsoup.parse(route).select("poi");
			positionPOIsList = new PositionPOIList(this.getResources().getDrawable(R.drawable.pushpin60), this, route);
			mapView.getOverlays().add(positionPOIsList);
			for (int i = 0; i < pois.size(); i++) {
				Element poiXML = pois.get(i);
				String[] location = poiXML.select("location").text().split(",");
				int lat = (int) (Double.parseDouble(location[0]) * 1E6);
				int lon = (int) (Double.parseDouble(location[1]) * 1E6);
				String name = poiXML.select("name").text();
				String description = poiXML.select("description").text();
				GeoPoint gp = new GeoPoint(lat, lon);
				OverlayItem item = new OverlayItem(gp, name, description);
				if (name.equals(getText(R.string.user).toString())) {
					Drawable icon = this.getResources().getDrawable(R.drawable.pushpin60_perso);
					icon.setBounds(
					    0 - icon.getIntrinsicWidth() / 2, 0 - icon.getIntrinsicHeight(), 
					    icon.getIntrinsicWidth() / 2, 0);
					item.setMarker(icon);
				}
				positionPOIsList.addMarker(item);
			}
			
			/*
			 * Si l'itinéraire dois être afficher alors le faire
			 */
			if (mustDraw) {
				drawRoute();
			}
			mapView.invalidate();
		}
	}
}
