package be.ac.umons.ProjetGL.BDS.UI;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import be.ac.umons.ProjetGL.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

public class ItinaryActivity extends TabActivity {
    private TabHost mTabHost;
    private String route;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.itinary);
        mTabHost = getTabHost();
        route = getIntent().getStringExtra("route");
        Intent intent1 = new Intent(ItinaryActivity.this, EarthMapActivity.class);
        intent1.putExtra("draw", true);
        intent1.putExtra("route", route);
        Drawable globe = getResources().getDrawable(R.drawable.ic_menu_globe);
        mTabHost.addTab(mTabHost.newTabSpec("map").setIndicator("Map", globe).setContent(intent1));

        Intent intent2 = new Intent(ItinaryActivity.this, InformationRouteActivity.class);
        intent2.putExtra("route", route);
        Drawable car = getResources().getDrawable(R.drawable.ic_menu_car);
        mTabHost.addTab(mTabHost.newTabSpec("information").setIndicator("Information", car).setContent(intent2));

        mTabHost.setCurrentTab(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case R.id.saveRoute:
            showDialog(0);
            return true;
    	case R.id.option:
            Intent intent = new Intent(ItinaryActivity.this, OptionActivity.class);
            startActivity(intent);
            return true;
    	case R.id.quitter:
            finish();
            return true;
    	case R.id.draw:
            Activity currentActivity = getLocalActivityManager().getActivity("map");
            if(currentActivity != null && currentActivity instanceof EarthMapActivity) {
                EarthMapActivity activity = (EarthMapActivity) currentActivity;
                if (activity.isRouteDisplayed())
                    activity.undrawRoute();
                else
                    activity.drawRoute();
            }
            return true;
    	}
    	return false;
    }

    protected Dialog onCreateDialog( int id ) {
        switch (id) {
        case 0:
            AlertDialog.Builder builder;
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.saveroute, (ViewGroup) findViewById(R.layout.itinary));

            TextView text = (TextView) layout.findViewById(R.id.nameroute);
            final EditText editText = (EditText) layout.findViewById(R.id.namerouteedit);
            builder = new AlertDialog.Builder(this);
            builder.setView(layout);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        saveRoute(editText.getText().toString());
                    }
                });
            return builder.create();
        }

        return null;
    }

    private void saveRoute(String name) {
    	FileOutputStream fOut = null;
        OutputStreamWriter osw = null;

        try {
            fOut = openFileOutput("favorite.xml", Context.MODE_APPEND);
            osw = new OutputStreamWriter(fOut);
            route = route.replaceFirst("<route name=\".*\">", "<route name =\"" + name + "\">");
            osw.write(route);
            osw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                osw.close();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
