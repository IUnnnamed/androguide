package be.ac.umons.ProjetGL.BDS.UI;

import be.ac.umons.ProjetGL.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
  
        final Button generatebutton = (Button) findViewById(R.id.generate);
        generatebutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, GenerateRouteActivity.class);
				startActivity(intent);
			}
		});
        
        final Button optionbutton = (Button) findViewById(R.id.option);
        optionbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, OptionActivity.class);
				startActivity(intent);
			}
		});
        
        
        final Button mapbutton = (Button) findViewById(R.id.earthmap);
        mapbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, EarthMapActivity.class);
				startActivity(intent);
			}
		});
        
        final Button displayPOIbutton = (Button) findViewById(R.id.displaypoi);
        displayPOIbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenuActivity.this, DisplayPOIActivity.class);
				startActivity(intent);
			}
		});
    }
}