package be.ac.umons.ProjetGL.BDS.UI;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import be.ac.umons.ProjetGL.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class InformationRouteActivity extends Activity {
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.informationroute);
		ListView list = (ListView) findViewById(R.id.listInfoRoute);

		String route = getIntent().getStringExtra("route");

		Document doc = Jsoup.parse(route);
		
		SimpleAdapter adapter = new SimpleAdapter(this, getInformations(doc), android.R.layout.simple_list_item_2,
				new String[]{"place", "move"}, new int[]{android.R.id.text1, android.R.id.text2});
		list.setAdapter(adapter);
	}

	private ArrayList<HashMap<String, String>> getInformations (Document doc) {
		ArrayList<HashMap<String,String>> toReturn = new ArrayList<HashMap<String,String>>();
		Elements name = doc.select("place");
		Elements move = doc.select("move");
		
		for (int i = 0; i < name.size(); i++) {
			HashMap<String, String> map = new HashMap<String, String> ();
			map.put("place", name.get(i).text());
			map.put("move", move.get(i).text());
			toReturn.add(map);
		}
		return toReturn;
	}
}
