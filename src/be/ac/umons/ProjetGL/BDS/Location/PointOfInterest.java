package be.ac.umons.ProjetGL.BDS.Location;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import be.ac.umons.ProjetGL.R;

import com.google.android.maps.GeoPoint;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.text.TextUtils;

public class PointOfInterest extends Address {

    private int duration = 0;
    private int nbVote = 0;
    private double price = 0;
    private double rank = 0;
    private HashMap<String, String> description = null;
    private String address = null;
    private String id = null;
    private String name = null;
    private ArrayList<String> tags = null;

    /**
     * Crée un objet PointOfInterest représentant un lieu sur une carte.
     * @param id l'id du poi
     * @param locale
     */
    public PointOfInterest (String id, Locale locale) {
    	this(id, locale, new GeoPoint(0,0));
    }

    /**
     * Crée un objet PointOfInterest représentant un lieu sur une carte, avec
     * sa position
     * @param id l'id du poi
     * @param locale la locale du poi (utilisé pour la langue des informations)
     * @param gp la position du poi
     */
    public PointOfInterest (String id, Locale locale, GeoPoint gp) {
        super(locale);
        this.id = id;
        setLatitude((double) gp.getLatitudeE6() / 1e6);
        setLongitude((double) gp.getLongitudeE6() / 1e6);
        tags = new ArrayList<String>();
        description = new HashMap<String, String>();
    }

    /**
     * Retourne l'identificateur de POI
     * @return l'id du poi
     */
    public String getId () {
    	return this.id;
    }

    /**
     * Retourne le nom du POI
     * @return le nom du poi
     */
    public String getName() {
    	return this.name;
    }

    /**
     * Returne le ranking du POI
     * @return la note du poi
     */
    public double getRank () {
    	return this.rank;
    }

    /**
     * Retourne le nombre de vote fait sur ce POI
     * @return le nombre de vote qu'à reçu le poi
     */
    public int getNbVote () {
    	return nbVote;
    }

    /**
     * Retourne un ArrayList<String> contenant les tags lié à ce POI
     * @return la liste des tags de ce poi
     */
    public ArrayList <String> getTags () {
    	return this.tags;
    }

    /**
     * Retourne le temps que l'on peu passer sur ce POI.
     * @return la durée du poi
     */
    public int getDuration() {
        return this.duration;
    }

    /**
     * Retourne le prix du POI. Exemple: le prix d'un ticket pour un musée.
     * @return le prix du poi
     */
    public double getPrice() {
        return this.price;
    }

    /**
     * Retourne la description de ce POI.
     * @return la description du poi
     */
    public String getDescription(Context context) {
        String lang = this.getLocaleString();
        String toReturn = this.description.get(lang);
        if (toReturn == null) {
            return context.getString(R.string.nodescription) + " " + this.name;
        }
        else
            return toReturn;
    }

    /**
     * Retourne l'adresse du POI
     * @return l'adresse du poi
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * Retourne la position du POI mis sous la forme d'un GeoPoint
     * @return la position du poi
     */
    public GeoPoint getGeoPoint() {
        int lat = (int) (this.getLatitude() * 1E6);
        int lon = (int) (this.getLongitude() * 1E6);
        return new GeoPoint(lat, lon);
    }

    /**
     * Modifie le nom du POI
     * @param name le nouveau nom
     */
    public void setName(String name) {
    	this.name = name;
    }

    /**
     * Modifie la note du POI
     * @param rank la nouvelle note
     */
    public void setRank (double rank) {
    	double temp = (this.rank * this.nbVote) + rank;
    	System.out.println(temp);
    	double newRank = temp / (double) (this.nbVote + 1);
    	System.out.println(newRank);
    	this.setNBVote(this.nbVote + 1);
    	this.rank = newRank;
    }

    /**
     * Modifie le nombre de vote du POI
     * @param nbVote le nouveau nombre de vote
     */
    public void setNBVote (int nbVote) {
    	this.nbVote = nbVote;
    }

    /**
     * Modifie la liste des tags du POI
     * @param tags
     */
    public void setTags (ArrayList<String> tags) {
    	this.tags = tags;
    }

    /**
     * Modifie la durée du POI
     * @param duration la nouvelle durée du poi
     */
    public void setDuration(int duration){
    	this.duration = duration;
    }

    /**
     * Modifie le prix du POI
     * @param price le nouveau prix
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Ajoute une description au POI
     * @param description la description du poi
     * @param lang la langue de la description
     */
    public void setDescription(String description, String lang) {
        this.description.put(lang.toLowerCase(), description);
    }

    /**
     * Modifie l'adresse du POI
     * @param address la nouvelle addresse
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Modifie la position du POI sur la carte
     * @param gp la nouvelle position
     */
    public void setGeoPoint(GeoPoint gp) {
        setLatitude((double) gp.getLatitudeE6() / 1e6);
        setLongitude((double) gp.getLongitudeE6() / 1e6);
    }

    public String toString() {
        String toReturn = "<poi>\n";
        toReturn += "<name>" + this.name + "</name>\n";
        toReturn += "<id>" + this.id + "</id>\n";
        toReturn += "<locale>" + this.getLocaleString() + "</locale>\n";
        for (String k : (Set<String>) this.description.keySet()) {
            toReturn += "<description lang=\"" + k + "\">\n";
            toReturn += "<text>" + description.get(k) + "</text>\n";
            toReturn += "</description>\n";
        }
        toReturn += "<address>" + this.address + "</address>\n";
        toReturn += "<location>" + this.getLatitude() + "," + this.getLongitude() + "</location>\n";
        toReturn += "<price>" + this.price + "</price>\n";
        toReturn += "<duration>" + this.duration + "</duration>\n";
        toReturn += "<rank>" + this.rank + "</rank>\n";
        toReturn += "<vote>" + this.nbVote + "</vote>\n";
        toReturn += "<tags>" + TextUtils.join(",", this.tags) + "</tags>\n";
        return toReturn + "</poi>\n";
    }

    /**
     * Crée un nouvel objet PointOfInterest avec sa description sous forme xml.
     * @param poi le poi sous forme xml
     * @return un objet PointOfInterest correspondant au xml
     */
    public static PointOfInterest reform (String poi) {
        PointOfInterest toReturn;
        Document doc = Jsoup.parse(poi);
        toReturn = new PointOfInterest(doc.select("id").text(), new Locale(doc.select("locale").text()));
        String[] var = doc.select("location").text().split(",");
        int lat = (int) (Double.parseDouble(var[0]) * 1E6);
        int lon = (int) (Double.parseDouble(var[1]) * 1E6);
        toReturn.setGeoPoint(new GeoPoint(lat, lon));
        toReturn.setName(doc.select("name").text());
        toReturn.setAddress(doc.select("address").text());
        toReturn.setPrice(Double.parseDouble(doc.select("price").text()));
        toReturn.setDuration(Integer.parseInt(doc.select("duration").text()));
        toReturn.setRank(Double.parseDouble(doc.select("rank").text()));
        toReturn.setNBVote(Integer.parseInt(doc.select("vote").text()));
        Elements desc = doc.select("description");
        for (Element e : desc) {
            toReturn.setDescription(e.select("text").text(), e.attr("lang"));
        }
        String[] tags = doc.select("tags").text().split(",");
        ArrayList<String> tags_ = new ArrayList<String>();
        Collections.addAll(tags_, tags);
        toReturn.setTags(tags_);

        return toReturn;
    }

    /**
     * Retourne la distance entre deux POI, cette distance est en mètre
     * @see Location#distanceBetween(double startLatitude, double startLongitude, double endLatitude, double endLongitude, float[] results)
     * @param poi1 le premier poi
     * @param poi2 le deuxieme poi
     * @return la distance a vol d'oiseau entre les deux poi en metre
     */
    public static double distance (PointOfInterest poi1, PointOfInterest poi2) {
        float[] results = new float[1];
        Location.distanceBetween(poi1.getLatitude(), poi1.getLongitude(), poi2.getLatitude(), poi2.getLongitude(), results);
        return results[0];
    }

    /**
     * Retourne la distance routière entre les deux POI, cette distance est en mètre.
     * @param poi1 le premier poi
     * @param poi2 le deuxieme poi
     * @return la distance routiere entre les deux poi en metre
     */
    public static double realDistance (PointOfInterest poi1, PointOfInterest poi2) {
        String url = getUrl(poi1.getLatitude(), poi1.getLongitude(),
                            poi2.getLatitude(), poi2.getLongitude(), poi1.getLocaleString());
        try {
            InputStream is = getConnection(url);
            Document doc = Jsoup.parse(is, "UTF-8", url);
            Elements desc = doc.select("description");
            return extractDistance(desc.last());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private static double extractDistance(Element description) {
        Pattern p = Pattern.compile("\\d\\.?\\d*(&#160;(km|m))");
        Matcher m = p.matcher(description.text());
        m.find();
        String[] temp = m.group(0).split("&#160;");
        double toReturn = Double.parseDouble(temp[0]);
        if(temp[1].equals("km")) {
            return toReturn * 1000;
        } else {
            return toReturn;
        }
    }

    /**
     *
     * @param fromLat la latitude du point de départ
     * @param fromLon la longitude du point de départ
     * @param toLat la latitude du point d'arrivée
     * @param toLon la longitude du point d'arrivée
     * @param locale la langue a utiliser pour receuillir les bonnes informations
     * @return une chaine de caractère correspondant à la requete a faire a google maps
     * pour receuillir des informations sur la trajet entre le point de départ et le point d'arrivée
     */
    public static String getUrl(double fromLat, double fromLon, double toLat, double toLon, String locale) {
    	String url = "http://maps.google.com/maps?f=d&hl=";
    	url += locale;
    	url += "&saddr=";// from
    	url += Double.toString(fromLat) + ",";
    	url += Double.toString(fromLon);
    	url += "&daddr=";// to
    	url += Double.toString(toLat) + ",";
    	url += Double.toString(toLon);
    	url += "&ie=UTF8&0&om=0&dirflg=w&output=kml";
    	return url;
    }

    /**
     * @return la langue utilisé par l'objet mise sous la forme d'un string de taille 2
     * (français : fr, anglais : en, ...)
     */
    public String getLocaleString () {
    	return getLocale().getISO3Language().substring(0, 2).toLowerCase();
    }

    /**
     * @param url l'url a partir de laquel faire une connection
     * @return un objet InputStream correspondant a la réponse de la connection
     */
    public static InputStream getConnection(String url) {
    	InputStream is = null;
    	try {
            URLConnection conn = new URL(url).openConnection();
            is = conn.getInputStream();
    	} catch (MalformedURLException e) {
            e.printStackTrace();
    	} catch (IOException e) {
            e.printStackTrace();
    	}
    	return is;
    }

    /**
     * @see Object#equals(Object o)
     */
    @Override
    public boolean equals (Object o) {
    	if (o == null) {
            return false;
    	} else if (o instanceof PointOfInterest) {
            PointOfInterest poi = (PointOfInterest) o;
            return this.id.equals(poi.getId());
    	} else {
            return false;
    	}
    }
}