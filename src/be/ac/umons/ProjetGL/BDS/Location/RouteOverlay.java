package be.ac.umons.ProjetGL.BDS.Location;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class RouteOverlay extends Overlay {
	private ArrayList<GeoPoint> gp;
	
	/**
	 * Crée un objet RouteOverlay avec un ArrayList<GeoPoint>.
	 * L'arrayList représente un itinéraire et sera utiliser
	 * pour qu'il soit tracer.
	 * @param gp
	 */
	public RouteOverlay(ArrayList<GeoPoint> gp) {
		this.gp = gp;
	}
	
	/**
	 * Permet de tracer la route sur la carte.
	 * @see com.google.android.maps.Overlay#draw(android.graphics.Canvas, com.google.android.maps.MapView, boolean)
	 */
	@Override
    public void draw(Canvas canvas, MapView mapv, boolean shadow){
        super.draw(canvas, mapv, shadow);

        Paint paint = new Paint();
        paint.setDither(true);
        paint.setColor(Color.BLUE);
        paint.setAlpha(120);
        paint.setStrokeWidth(5);
        Projection projection = mapv.getProjection();

        for (int i = 0; i < gp.size() - 1; i++){
            Point point = new Point();
            Point point2 = new Point();

            projection.toPixels(gp.get(i), point);
            projection.toPixels(gp.get(i + 1), point2);

            canvas.drawLine(point.x, point.y, point2.x, point2.y, paint);
        }
    }
}
