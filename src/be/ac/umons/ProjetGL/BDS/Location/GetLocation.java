package be.ac.umons.ProjetGL.BDS.Location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class GetLocation {
	
	/**
	 * Retourne la  position de l'utilisateur en utilisant le réseau
	 * @param context
	 * @return la position de l'utilisateur
	 */
	public static Location getLocationNetwork (Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		LocationListener mlocListener = getListener(context, "Network");
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
		
		return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	}
	
	/**
	 * Retourne la  position de l'utilisateur en utilisant le le GPS
	 * @param context
	 * @return la position de l'utilisateur
	 */
	public static Location getLocationGPS (Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		LocationListener mlocListener = getListener(context, "GPS");
		locationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
		
		return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}
	
	private static LocationListener getListener (final Context context, String provider) {
		LocationListener toReturn = new LocationListener() {
			public void onLocationChanged(Location loc) {
				loc.getLatitude();
				loc.getLongitude();
			}
			 
			public void onProviderDisabled(String provider) {
				Toast.makeText(context.getApplicationContext(), provider + " Disabled", Toast.LENGTH_SHORT).show();
			}
			 
			public void onProviderEnabled(String provider) {
				Toast.makeText(context.getApplicationContext(), provider + " Enabled", Toast.LENGTH_SHORT).show();
			}
			
			public void onStatusChanged(String provider, int status, Bundle extras) {}
			 
		};
		
		return toReturn;
	}
}