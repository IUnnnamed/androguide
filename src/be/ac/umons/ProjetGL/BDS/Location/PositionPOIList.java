package be.ac.umons.ProjetGL.BDS.Location;
 
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
 
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import be.ac.umons.ProjetGL.R;
import be.ac.umons.ProjetGL.BDS.UI.InformationPOIActivity;
 
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
 
public final class PositionPOIList extends ItemizedOverlay<OverlayItem> {
 
    private ArrayList<OverlayItem> overlay = new ArrayList<OverlayItem>();
    private String route = null;
    private Context context = null;
 
    /**
     * Crée un objet PositionPOIList étant un ItemizedOverlay,
     * avec un context et un itinéraire. L'itinéraire va permettre
     * d'aller chercher des informations sur chacun des OverlayItem
     * caractérisant les POI.
     * @param defaultMarker
     * @param context
     * @param route
     */
    public PositionPOIList(Drawable defaultMarker, Context context, String route) {
        super(boundCenterBottom(defaultMarker));
        this.context = context;
        this.route = route;
    }
 
    /**
     * @see ItemizedOverlay#addMarker(OverlayItem overlay)
     */
    public void addMarker(OverlayItem overlay) {
        this.overlay.add(overlay);
        populate();
    }
 
    /**
     * @see ItemizeOverlay#createItem(int i)
     */
    protected OverlayItem createItem(int i) {
        return this.overlay.get(i);
    }
 
    /**
     * @see ItemizedOverlay#size()
     */
    public int size() {
        return this.overlay.size();
    }
 
    /**
     * @see ItemizedOverlay#onTap(int index)
     */
    protected boolean onTap(int index) {
    	final Elements pois = Jsoup.parse(route).select("poi");
    	final Context context = this.context;
    	final int i = index;
    	OverlayItem item = this.overlay.get(i);
    	AlertDialog.Builder dialog = new AlertDialog.Builder(context);
    	dialog.setTitle(item.getTitle());
    	if (!pois.get(i).select("name").text().equals(context.getText(R.string.user).toString())) {
    		dialog.setMessage(item.getSnippet());
    		dialog.setPositiveButton(context.getText(R.string.moreinfo), new DialogInterface.OnClickListener(){
    			public void onClick(DialogInterface dialog, int which) {
    				Intent intent = new Intent(context, InformationPOIActivity.class);
    				intent.putExtra("actualPOI", pois.get(i).toString());
    				context.startActivity(intent);
    			}});
    	}
    	dialog.show();
    	return true;
    }
 
}
