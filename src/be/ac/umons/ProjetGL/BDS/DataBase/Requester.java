package be.ac.umons.ProjetGL.BDS.DataBase;


import java.util.ArrayList;

import com.google.android.maps.GeoPoint;

public interface Requester {
	public ArrayList <String> getIds();
    public ArrayList <String> getNames ();
    public ArrayList <GeoPoint> getLocations ();
    public String get (String attribute, String id)
    		throws Exception;
    public void setValue (String attribute, String value, String id)
    		throws Exception;
}