package be.ac.umons.ProjetGL.BDS.DataBase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.android.maps.GeoPoint;

public class OnlineRequester implements Requester {
	private static final HashMap<String, ArrayList<String>> BDD = new HashMap<String, ArrayList<String>>();
    private Connection connection=null;
    private String url;

    /**
     * Crée un nouvel objet OnlineRequester dont ses requêtes seront envoyées à l'url donnée
     * @param url l'adresse pour envoyer les requêtes SQL
     */
    public OnlineRequester (String url){
        this.url = url;
        BDD.put("Descriptions", new ArrayList <String> (Arrays.asList ("Id", "Language", "Description")));
        BDD.put("Infos",  new ArrayList <String> (Arrays.asList ("Id", "Price", "Duration")));
        BDD.put("POI", new ArrayList <String> (Arrays.asList ("Id", "Name", "Longitude", "Latitude", "Address")));
        BDD.put("Ranking", new ArrayList <String> (Arrays.asList ("Id", "Rank", "NBVote")));
        BDD.put("TAG", new ArrayList <String> (Arrays.asList ("Id", "Tag")));
    }

    /**
     * Crée la connexion vers le site ou envoyer les requêtes SQL
     * @param context permet de savoir si une connexion internet est possible
     * @throws Exception si aucune connexion n'est possible (ni WIFI, ni MOBILE)
     */
	public void connect (Context context) throws Exception {
    	boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected()) {
                    haveConnectedWifi = true;
                    break;
                }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected()) {
                    haveConnectedMobile = true;
                    break;
                }
        }
        
        if (haveConnectedWifi || haveConnectedMobile) {
            this.connection = Jsoup.connect(this.url);
        } else {
            throw new Exception("Connection fail");
        }
    }

	/**
	 * Retourne un ArrayList<String> contenant les identificateurs des POI
	 * @return la liste des identificateurs de chacun des poi de la BDD
	 */
	public ArrayList<String> getIds() {
        ArrayList <String> toReturn = new ArrayList <String> ();
        String request = "SELECT Id FROM POI";
        Matcher matcher = this.getMatcher(request);
        while (matcher.find()) {
            String name = matcher.group(2).split(":")[1];
            toReturn.add(name.replaceAll("\"", ""));
        }
        return toReturn;
    }
	
	/**
	 * Retourne un Array<String> contenant les noms des POI
	 * @return la liste des noms de chacun des poi de la BDD
	 */
    public ArrayList <String> getNames () {
        ArrayList <String> toReturn = new ArrayList <String> ();
        String request = "SELECT Name FROM POI";
        Matcher matcher = this.getMatcher(request);
        while (matcher.find()) {
            String name = matcher.group(2).split(":")[1];
            toReturn.add(name.replaceAll("\"", ""));
        }
        return toReturn;
    }
    
    /**
     * Retourne un ArrayList<String> contenant les tags que peuvent avoir les POI
     * @return la liste de l'ensemble des tags de chacun des poi de la BDD
     */
    public ArrayList<String> getTags () {
        ArrayList <String> toReturn = new ArrayList <String> ();
        String request = "SELECT DISTINCT TAG FROM TAG";
        Matcher matcher = this.getMatcher(request);
        while (matcher.find()) {
            String name = matcher.group(2).split(":")[1];
            toReturn.add(name.replaceAll("\"", ""));
        }
        return toReturn;
    }

    /**
     * Retourne un ArrayList<GeoPoint> contenant la position des POI
     * @return la liste des positions de chacun des poi de la BDD
     */
    public ArrayList <GeoPoint> getLocations () {
        ArrayList <GeoPoint> toReturn = new ArrayList <GeoPoint> ();
        String request = "SELECT Longitude, Latitude FROM POI";
        Matcher matcher = this.getMatcher(request);
        while (matcher.find()) {
            String[] longLat = matcher.group(2).split(",");
            String longStr = longLat[0].split(":")[1];
            String latStr = longLat[1].split(":")[1];
            int x = (int) (Float.parseFloat(longStr.replaceAll("\"", "")) * 1e6);
            int y = (int) (Float.parseFloat(latStr.replaceAll("\"", "")) * 1e6);
            toReturn.add(new GeoPoint(x, y));
        }
        return toReturn;
    }


    /**
     * Retourne la valeur de l'attribut que l'on souhaite pour le POI ayant comme
     * identificateurs id. Exemple get("Name", "1") retourne "Pentagone"
     * @param attribute l'attribut dont on veut une valeur
     * @param id l'identificateur du poi
     * @return la valeur de l'attribut souhaiter pour le poi sélectionné
     */
	public String get (String attribute, String id)
    throws Exception {
        String db = null;

        for (String key : (Set <String>) BDD.keySet()) {
            ArrayList <String> temp = (ArrayList <String>) BDD.get(key);
            if (temp.contains(attribute)) {
                db = key;
                break;
            }
        }

        if (db == null) { throw new Exception ("Attribute " + attribute + " doesn't exist"); }

        String toReturn = "";
        String request = "SELECT " + attribute + " FROM " + db + " WHERE Id=(" +
                id + ")";
        Matcher matcher = this.getMatcher(request);
        int count = 0;
        while (matcher.find()) {
            count++;
            toReturn += matcher.group(2).split(":")[1].replaceAll("\"", "");
            if (count > 1)
                toReturn += ";";
        }
        return toReturn;
    }

    /**
     * Permet de renvoyer l'ensemble des attributs souhaité pour l'ensemble des poi de la BDD en une seule requête
     * @param attrtab un tableau contenant les attributs souhaité
     * @param filter une liste contenant les conditions de la requête
     * @return une matrice représentant la réponse de la requête
     * @throws Exception si attrtab est vide
     */
    @SuppressWarnings("unchecked")
	public String[][] get (String[] attrtab, ArrayList<String> filter) throws Exception {
       	if (attrtab.length > 0) {
       		ArrayList<String> dbsattr = new ArrayList<String>();
       		ArrayList<String> dbsfilter = new ArrayList<String>();
	    	String sql = "SELECT ";
	    	boolean idAdded = false;
	    	ArrayList<String> attrfield = new ArrayList<String>();
	    	ArrayList<String> filterfield = new ArrayList<String>();
	    	for (String s : attrtab)
	    		attrfield.add(s);
	    	for (String f : filter)
	    		filterfield.add(f.split(" ")[0]);
	    	for (String s : attrfield) {
	    		for (String key : (Set <String>) BDD.keySet()) {
	                ArrayList <String> temp = (ArrayList <String>) BDD.get(key);
	                if (temp.contains(s)) {
	                	if (!idAdded){
	                		idAdded = true;
	                		sql += key + '.' + "Id";
	                	}
	                    dbsattr.add(key);
	            		sql += "," + key + '.' + s;
	                    break;
	                }
	            }
	    	}
	    	for (String s : filterfield){
	    		for (String key : (Set <String>) BDD.keySet()) {
	                ArrayList <String> temp = (ArrayList <String>) BDD.get(key);
	                if (temp.contains(s)) {
	                	dbsfilter.add(key);
	                	break;
	                }
	    		}
	    	}
	    	sql += " FROM " + dbsattr.get(0);
	    	ArrayList<String> filtertabalrdyused = new ArrayList<String>();
	    	filtertabalrdyused.add(dbsattr.get(0));
	    	for (int i = 1; i < dbsattr.size(); i++) {
	    		if (!filtertabalrdyused.contains(dbsattr.get(i))){
	    			sql += ',' + dbsattr.get(i);
	    			filtertabalrdyused.add(dbsattr.get(i));
	    		}
	    	}
	    	for (int i = 0; i < dbsfilter.size(); i++) {
	    		if (!filtertabalrdyused.contains(dbsfilter.get(i))){
	    			sql += ',' + dbsfilter.get(i);
	    		} else {
	    			filtertabalrdyused.add(dbsfilter.get(i));
	    		}
	    	}
	    	sql += " WHERE ";
	    	for (int i = 1; i < dbsattr.size(); i++) {
	    		sql += dbsattr.get(0) + ".Id=" + dbsattr.get(i) + ".Id AND ";
	    	}
	    	ArrayList<String> filtertabidalrdychecked = (ArrayList<String>) dbsattr.clone();
	    	for (int i = 0; i < dbsfilter.size(); i++) {
	    		if (!filtertabidalrdychecked.contains(dbsfilter.get(i))){
	    			sql += dbsattr.get(0) + ".Id=" + dbsfilter.get(i) + ".Id AND ";
	    		} else {
	    			filtertabidalrdychecked.add(dbsfilter.get(i));
	    		}
	    	}
	    	for (int i = 0; i < dbsfilter.size(); i++) {
	    		sql += dbsfilter.get(i) + '.' + filter.get(i) + " AND ";
	    	}
	    	sql += "1";
	    	
	    	ArrayList<ArrayList<String>> temp = request(sql);
	    	String[][] toReturn = new String[temp.size()][];
	    	for (int i = 0; i < toReturn.length; i++)
	    		toReturn[i] = temp.get(i).toArray(new String[0]);
	    	
	    	return toReturn;
    	} else {
    		throw new Exception("You must select at least one attribute.");
    	}    
    }
    
    private ArrayList<ArrayList<String>> request(String sql) {
    	ArrayList<ArrayList<String>> toReturn = new ArrayList<ArrayList<String>>();
    	Matcher matcher = this.getMatcher(sql);
    	while (matcher.find()) {
    		String[] temp0 = matcher.group(2).split("\",\"");
    		ArrayList<String> temp1 = new ArrayList<String>();
    		for (String s : temp0) {
    			temp1.add(s.split(":")[1].replaceAll("\"", ""));
    		}
    		toReturn.add(temp1);
    	}
    	return toReturn;
    }
    
    /**
     * Permet de modifier la valeur d'un attribut d'un certain poi
     * @param attribute l'attribut à modifier
     * @param value la valeur à mettre pour l'attribut souhaité
     * @param id l'identificateur du poi que l'on veut modifier
     */
	public void setValue (String attribute, String value, String id)
    throws Exception {
    	String db = null;

        for (String key : (Set <String>) BDD.keySet()) {
            ArrayList <String> temp = (ArrayList <String>) BDD.get(key);
            if (temp.contains(attribute)) {
                db = key;
                break;
            }
        }

        if (db == null) { throw new Exception ("Attribute " + attribute + " doesn't exist"); }
        
        String request = "UPDATE " + db + " SET " + attribute + "=" + value +
        		" WHERE Id=" + id + "";
        getMatcher(request);
    }

    /* 
     * Methode renvoyant un Matcher pour une certaine requete, utilisé pour chercher
     * facilement une valeur d'un attribut sur le retour de la requete envoyée.
	 */
    private Matcher getMatcher (String request) {
        for (int i = 0; i < 10; i++) {
            try {
                Document page = this.connection.data("request", request).post();
                String body = decode(page.body().text());
                
                Pattern pattern = Pattern.compile("(.*?)*\\{(.*?)\\}.*?");
                return pattern.matcher(body);
            } catch (IOException ioe) {}
        }
        return null;
    }

    /*
     *  Remplace les caractère non reconnu en leur semblable.
     */
    private String decode (String s) {
        String toReturn = new String(s);
        toReturn = toReturn.replaceAll("\\\\u0092", "'");
        toReturn = toReturn.replaceAll("\\\\u00ad", "");
        toReturn = toReturn.replaceAll("\\\\u00e2", "â");
        toReturn = toReturn.replaceAll("\\\\u00e0", "à");
        toReturn = toReturn.replaceAll("\\\\u00e7", "ç");
        toReturn = toReturn.replaceAll("\\\\u00e8", "è");
        toReturn = toReturn.replaceAll("\\\\u00e9", "é");
        toReturn = toReturn.replaceAll("\\\\u00ef", "î");
        toReturn = toReturn.replaceAll("\\\\u00f4", "ô");
        toReturn = toReturn.replaceAll("\\\\u00fc", "û");
        toReturn = toReturn.replaceAll("\\\\u00f9", "ù"); 
        return toReturn;
    }
}